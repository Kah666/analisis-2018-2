module.exports = function(sequelize, DataTypes) {
	return sequelize.define('locaciones',{
		locacion_id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
		},

		coordenadas: {
			type: DataTypes.STRING,
		},

		nombre: {
			type: DataTypes.STRING,
		},

		imagen: {
			type: DataTypes.STRING,
		},

		ultimoincendio: {
			type: DataTypes.DATEONLY,
		}
	}, {
		tableName: 'locaciones',
		timestamps: false,
	}
	);
}