const locaciones = sequelize.import('locaciones',require('./locaciones'));

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('eventos',{
		id_evento: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
		},

        id_locacion: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},

		ind_humed_combus: {
			type: DataTypes.INTEGER,
		},

		ind_prob_ignic: {
			type: DataTypes.INTEGER,
		},

		ind_riesgo_incen: {
			type: DataTypes.INTEGER,
		},

		fecha: {
			type: DataTypes.DATEONLY,
		}
	}, {
		tableName: 'eventos',
		timestamps: false,
	}
	);

eventos.belongsTo(locaciones, {foreignKey: 'locacion_id'});

}