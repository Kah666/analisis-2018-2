const express = require('express');
const router = express.Router();
const sequelize = require('./config');

const locaciones = sequelize.import('locaciones', require('./modelos/locaciones'));
/*const eventos = sequelize.import('eventos', require('./modelos/eventos')); */

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

sequelize.authenticate().then(() => {
    console.log('Exito!');
    }).catch(err => {
        console.log('Error TwT', err);
    })

router.get('/get_locacion', (req,res) => {
    locaciones.findAll()
    .then(rows => {
        return res.status(200).json({
            data: rows,
        })
    })
})

router.get('/get_locacion_ordenada', (req,res) => {
    locaciones.findAll({
        order: [
          ['ultimoincendio', 'DESC']
        ]
      })
    .then(rows => {
        return res.status(200).json({
            data: rows,
        })
    })
})

/*router.get('/get_ultimos_eventos', (req,res) => {
    eventos.findAll({
        where: {
          locacion_id: 2
        }
      }
        {
        order: [
          ['fecha', 'DESC']
        ]
      },
      {
          limit: 3
        },
        )
    .then(rows => {
        return res.status(200).json({
            data: rows,
        })
    })
}) */  

/*

router.get('/entradas', (req, res, next) =>{
    db.any('SELECT * FROM locaciones')
        .then(function (data) {
            res.status(200)
            .json({
                status: 'success',
                data: data,
                message: 'Todos los lugares registrados!'
            });
        })
    .catch(function (err) {
        console.log("malio sal :C")
        return next(err);
    });
});



/*
router.get('/', (req, res) =>{
    res.send('api works');
});

router.get('/entradas', (req, res, next) =>{
    db.any('SELECT * FROM imagenes')
        .then(function (data) {
            res.status(200)
            .json({
                status: 'success',
                data: data,
                message: 'Todos los lugares registrados!'
            });
        })
    .catch(function (err) {
        console.log("malio sal :C")
        return next(err);
    });
});

router.post('/entrada',(req, res)=>{
    var query = "INSERT INTO peliculas (titulo, comentario, fecha_estreno, calificacion) VALUES('"+req.body.titulo+"', '"+req.body.comentario+"', '"+req.body.Fecha+"', '"+req.body.calif+"')";
    db.query(query, function(err, rows){
        if(err){
            res.status(500).send({
                body:{
                    result:"error"
                }
            });
        }
        else{
            return res.status(200).send({
                body:{
                    result:"OK"
                }
            });
        }
    });
});

router.post('/entrada_d', (req, res, next) => {
    var ID = parseInt(req.body.id_peli);
    db.result( 'DELETE FROM peliculas WHERE id_peli = $1', ID)
      .then(function (result) {
        res.status(200)
          .json({
            status: 'success',
            message: `Removed ${result.rowCount} peli`
          });

      })
      .catch(function (err) {
        return next(err);
      });
  });*/


module.exports = router;