import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class EntradasService {
  constructor(private http: Http) { }
  
  getEntradas(){
    return this.http.get('http://localhost:3000/api/v1/get_locacion').pipe(map(res => res.json()));
  };

  getUltimosDatos(){
  	return this.http.get('http://localhost:3000/api/v1/get_locacion_ordenada').pipe(map(res => res.json()));
  }
}