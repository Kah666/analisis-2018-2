import { Component, OnInit } from '@angular/core';
import { EntradasService } from '../servicios/entradas.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css',]
})
export class FeedComponent implements OnInit {
	places: any = [];

	title = 'Locación X';

	constructor(private entradasService: EntradasService) { }
	data: any = [];
	ngOnInit() {
		this.entradasService.getEntradas().subscribe(rows =>{ 
			this.places = rows.data; 
			console.log(this.data);
		});
	}
}
