import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedComponent } from './feed/feed.component';
import { HomeComponent } from './home/home.component';
import { UltimosDatosComponent} from './ultimos-datos/ultimos-datos.component';


const routes: Routes = [
	{
		path: 'entradas', component: FeedComponent //Zonas de mayor riesgo
	},
	{
		path: 'entradas', component: UltimosDatosComponent //UltimosDatos
	},
	
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
