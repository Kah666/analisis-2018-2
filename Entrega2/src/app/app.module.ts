import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FeedComponent } from './feed/feed.component';

import { HttpModule } from '@angular/http';
import { EntradasService } from './servicios/entradas.service';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';

import {MatCardModule} from '@angular/material/card';
import { UltimosDatosComponent } from './ultimos-datos/ultimos-datos.component';

import { RouterModule,Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FeedComponent,
    UltimosDatosComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    RouterModule.forRoot([
    {
      path: 'ultimos_datos',
      component: UltimosDatosComponent
    },
    {
      path: 'feed',
      component: FeedComponent
    },
    {
      path: '',
      component: HomeComponent
    }

      ])
  ],
  providers: [
    EntradasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
