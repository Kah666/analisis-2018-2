import { Component, OnInit } from '@angular/core';
import { EntradasService } from '../servicios/entradas.service';

@Component({
  selector: 'app-ultimos-datos',
  templateUrl: './ultimos-datos.component.html',
  styleUrls: ['./ultimos-datos.component.css']
})
export class UltimosDatosComponent implements OnInit {
	places: any = [];

	title = 'LastDatos';
	
	constructor(private entradasService: EntradasService) { }
	data: any = [];
	ngOnInit() {
		this.entradasService.getUltimosDatos().subscribe(rows =>{ 
			this.places = rows.data; 
			console.log(this.data);
		});
	}
}
