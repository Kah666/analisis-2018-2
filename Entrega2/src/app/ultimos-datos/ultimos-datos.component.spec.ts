import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimosDatosComponent } from './ultimos-datos.component';

describe('UltimosDatosComponent', () => {
  let component: UltimosDatosComponent;
  let fixture: ComponentFixture<UltimosDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UltimosDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimosDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
