# Proyecto

## Instrucciones para ejecutar API

Ejemplo para conectarse a una API local mediante Angular y as� realizar cambios u obtener data de una Base de Datos local, utilizando sequelize.

#Instrucciones

##Clonar repositorio:

git clone https://Kah666@bitbucket.org/Kah666/analisis-2018-2.git
Entrar al directorio e instalar dependencias desde el archivo package.json:

npm install
Importar la base de datos a postgres.

Editar el archivo server/sequelize.js con las credenciales de su base de datos local.

##Compilar y levantar su servidor:

ng build && node server
Dirigirse a http://localhost:4200/ para ver la p�gina.